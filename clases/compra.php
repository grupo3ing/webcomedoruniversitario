<?php

class Compra {

	private $id_mp;
	private $estado;
	private $dni;
	private $fecha;
	private $cantidad;
	private $vianda_precio;


	public function __construct() {

	}

	public function rellenar($id_mp, $estado, $dni, $cantidad, $vianda_precio){
		$this->id_mp = $id_mp;
		$this->estado = $estado;
		$this->dni = $dni;
		$this->cantidad = $cantidad;
		$this->vianda_precio = $vianda_precio;
	}

	static public function crear($id_mp, $estado, $dni, $cantidad, $vianda_precio) {
		$instance = new self();
        $instance->rellenar($id_mp, $estado, $dni, $cantidad, $vianda_precio);
        return $instance;
	}

	public function getId(){
		return $this->id_mp;
	}

	public function setId($id){
		$this->id_mp = $id;
	}

	public function getEstado(){
		return $this->estado;
	}

	public function setEstado($estado){
		$this->estado = $estado;
	}

	public function getDni(){
		return $this->dni;
	}
	
	public function setDni($dni){
		$this->dni = $dni;
	}

	public function getFecha(){
		return $this->fecha;
	}
	
	public function setFecha($fecha){
		$this->fecha = $fecha;
	}

	public function getCantidad(){
		return $this->cantidad;
	}
	
	public function setCantidad($cantidad){
		$this->cantidad = $cantidad;
	}

	public function getViandaPrecio(){
		return $this->vianda_precio;
	}
	
	public function setViandaPrecio($vianda_precio){
		$this->vianda_precio = $vianda_precio;
	}

	public function buscar_datos(){
		include '../sql.php';

		/*$year = date("Y");
	    $month = date("n") + 1;
	    $day = date("d");
	    if ($day >= 1 && $day < 20) {
	    	$month--;
	    }
	    $min = $year . "-" . $month . "-" . "20";
	    $max = $year . "-" . $month . "-" . "28";*/

        $sql = sprintf("SELECT * FROM `compras` WHERE id_mp = '%s'", mysqli_real_escape_string($mysqli,$this->id_mp));
        if (!$result = $mysqli->query($sql)) {
	        printf("Errormessage: %s\n", $mysqli->error);
        } else {
            $row = $result->fetch_assoc();
            $this->estado = $row['estado'];
			$this->dni = $row['dni'];
			$this->fecha = $row['fecha'];
			$this->cantidad = $row['cantidad'];
			$this->vianda_precio = $row['vianda_precio'];
        }
	}

	public function guardar(){
		include '../sql.php';
		$sql = sprintf("INSERT INTO compras (`id_mp`,`estado`,`dni`,`cantidad`,`vianda_precio`) VALUES ('%s','%s','%s','%s','%s')", 
            $this->id_mp, $this->estado, $this->dni, $this->cantidad, $this->vianda_precio);
        if (!$result = $mysqli->query($sql)) {
	        printf("Errormessage: %s %s\n", $sql ,$mysqli->error);
	        return false;
        } else {
        	return true;
        }
	}

	static public function buscarUltimaCompraxDni($dni){
		include '../sql.php';
		$sql = sprintf("SELECT * FROM `compras` WHERE dni = '%s' ORDER BY fecha DESC LIMIT 1", mysqli_real_escape_string($mysqli,$dni));
		$compra = new Compra();
        if (!$result = $mysqli->query($sql)) {
	        printf("Errormessage: %s\n", $mysqli->error);
        } else {
            $row = $result->fetch_assoc();
            $compra->setId($row['id_mp']);
            $compra->buscar_datos();           
        }
        return $compra;
	}

	static public function calcularPrecio($dni){
		require_once('cliente.php');
	    $cliente = new Cliente($dni);
	    if ($cliente->existe()) {
			include '../sql.php';
			$sql = "SELECT * FROM `viandas` ORDER BY fecha DESC LIMIT 1";
	        if (!$result = $mysqli->query($sql)) {
	            printf("Errormessage: %s\n", $mysqli->error);
	        } else {
	            $row = $result->fetch_assoc();
	            $precio = $row['precio'];
	        }

	        if($cliente->getBeca() == 'media') {
	            $precio = $precio / 2;
	        }

	        $year = date("Y");
	        $month = date('n', strtotime("+1 months", strtotime($year."-".date("n")."-01")));

	        if ($month == 1) {
	        	$year++;
	        }
	        

	        $json = file_get_contents('http://nolaborables.com.ar/api/v2/feriados/' . $year . '?formato=mensual');
	        $data = json_decode($json,true);

	        $feriados = [];

	        foreach(array_keys($data[$month-1]) as $feriado) {
	            array_push($feriados,$feriado);
	        }

	        $sql = "SELECT `fecha` FROM `nolaborables` WHERE MONTH(fecha) = '" . $month . "' AND YEAR(fecha) = '" . $year . "';";
	        if (!$result = $mysqli->query($sql)) {
	            printf("Errormessage: %s\n", $mysqli->error);
	        } else {
	            while($row = $result->fetch_assoc()) {
	                array_push($feriados,date_parse($row['fecha'])['day']);
	            }
	        }

	        $day = date("N", strtotime($year . "-" . $month . "-01")); // del 1 (Lunes) al 7 (Domingo) 
	        $day = ($day == 7) ? 0 : $day; // Domingo es 0
	        $daysm = date("t", strtotime($year . "-" . $month . "-01"));

	        $findesemana = [];

	        for( $i = 1; $i <= $daysm; $i++ ){
	            if($day == 0 || $day == 6) {
	                array_push($findesemana,$i);
	            }
	            $day = ($day == 6) ? 0 : $day+1;
	        }

	        $laborable = [];
	        $cantidad = 0;
	        for($i = 1; $i <= $daysm; $i++){
	            if(!in_array($i,$feriados) && !in_array($i,$findesemana)) {
	                $cantidad++;
	                array_push($laborable,$i);
	            }
	        }
	        $res = array("precio_unitario"=>$precio, "cantidad"=>$cantidad, "precio_total"=>$precio*$cantidad, "laborable"=>$laborable);
	        return $res;
	    }
	}

	static public function getDatos($mes){
		include '../sql.php';
        $year = date("Y");
        $month = $mes;

        if ($month == 1) {
        	$year++;
        }

        $json = file_get_contents('http://nolaborables.com.ar/api/v2/feriados/' . $year . '?formato=mensual');
        $data = json_decode($json,true);

        $feriados = [];

        foreach(array_keys($data[$month-1]) as $feriado) {
            array_push($feriados,$feriado);
        }

        $sql = "SELECT `fecha` FROM `nolaborables` WHERE MONTH(fecha) = '" . $month . "' AND YEAR(fecha) = '" . $year . "';";
        if (!$result = $mysqli->query($sql)) {
            printf("Errormessage: %s\n", $mysqli->error);
        } else {
            while($row = $result->fetch_assoc()) {
                array_push($feriados,date_parse($row['fecha'])['day']);
            }
        }

        $day = date("N", strtotime($year . "-" . $month . "-01")); // del 1 (Lunes) al 7 (Domingo) 
        $day = ($day == 7) ? 0 : $day; // Domingo es 0
        $daysm = date("t", strtotime($year . "-" . $month . "-01"));

        $findesemana = [];

        for( $i = 1; $i <= $daysm; $i++ ){
            if($day == 0 || $day == 6) {
                array_push($findesemana,$i);
            }
            $day = ($day == 6) ? 0 : $day+1;
        }

        $laborable = [];
        $cantidad = 0;
        for($i = 1; $i <= $daysm; $i++){
            if(!in_array($i,$feriados) && !in_array($i,$findesemana)) {
                $cantidad++;
                array_push($laborable,$i);
            }
        }
        $res = array("laborable"=>$laborable);
        return $res;
    }
}