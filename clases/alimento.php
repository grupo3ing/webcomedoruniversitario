<?php

class Alimento {
	private $tipo;
	private $nombre;

	public function __construct($nombre, $tipo){
		$this->nombre = $nombre;
		if($tipo != "") {
			$this->tipo = $tipo;
		} else {
			include '../sql.php';
			$sql = "SELECT tipo FROM alimentos WHERE nombre = '" . $nombre . "' AND tipo != 'postre' AND tipo != 'salsa' AND tipo != 'guarnicion'";
			$result = $mysqli->query($sql);
			$row = $result->fetch_assoc();
			$this->tipo = $row['tipo'];
		}
	}

	public function getNombre(){
		return $this->nombre;
	}
}