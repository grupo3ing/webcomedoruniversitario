<?php

class Beca {
    private $dni;
    private $periodo;
    private $tipo;

    public function __construct($dni, $periodo, $tipo) {
        $this->dni = $dni;
        $this->periodo = $periodo;
        $this->tipo = $tipo;
    }

    public function getDni() {
        return $this->dni;
    }

    public function getPeriodo() {
        return $this->periodo;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function __toString() {
        return "Beca ["
                . " dni=" . $this->dni
                . ", periodo=" . $this->periodo
                . ", tipo=" . $this->tipo
                . " ]";
    }
}