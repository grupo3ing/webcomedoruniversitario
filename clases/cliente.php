<?php

class Cliente {
    private $dni;
    private $legajo;
    private $nombre;
    private $apellido;
    private $tipo;
    private $email;
    private $direccion;
    private $telefono;
    private $estado;
    private $documentacion;
    private $becas;
    private $password;
    private $existe;
    private $token;

    public function __construct($dni) {
        $this->dni = $dni;
        $this->becas = [];
        $this->buscardatos();
    }

    public function getDni() {
        return $this->dni;
    }

    public function setDni($dni) {
        $this->dni = $dni;
    }

    public function getLegajo() {
        return $this->legajo;
    }

    public function setLegajo($legajo) {
        $this->legajo = $legajo;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function getApellido() {
        return $this->apellido;
    }

    public function setApellido($apellido) {
        $this->apellido = $apellido;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getDireccion() {
        return $this->direccion;
    }

    public function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    public function getTelefono() {
        return $this->telefono;
    }

    public function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    public function getEstado() {
        return $this->estado;
    }

    public function setEstado($estado) {
        $this->estado = $estado;
    }

    public function getDocumentacion() {
        return $this->documentacion;
    }

    public function setDocumentacion($documentacion) {
        $this->documentacion = $documentacion;
    }

    public function getBeca() {
        foreach ($this->becas as $beca) {
            if ($beca->getPeriodo() == date("Y")) {
                return $beca->getTipo();
            }
        }
        return "no";
    }

    public function getBecas() {
        return $this->becas;
    }

    public function setBeca($beca) {
        $this->beca = $beca;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
    }
  
    public function existe() {
        return $this->existe; 
    }

    public function __toString() {
        return "Cliente ["
                . " dni=" . $this->dni
                . ", legajo=" . $this->legajo
                . ", nombre=" . $this->nombre
                . ", apellido=" . $this->apellido
                . ", tipo=" . $this->tipo
                . ", email=" . $this->email
                . ", direccion=" . $this->direccion
                . ", telefono=" . $this->telefono
                . ", estado=" . $this->estado
                . ", documentacion=" . $this->documentacion
                . ", beca=" . $this->becado()
                . ", password=" . $this->password
                . " ]";
    }

    function buscardatos() {
        $this->existe = false;
        include '../sql.php';
        include_once 'beca.php';
        $sql = sprintf("SELECT * FROM `clientes` WHERE dni = '%s'", 
            mysqli_real_escape_string($mysqli,$this->dni) 
        );
        if (!$result = $mysqli->query($sql)) {
	        printf("Errormessage: %s\n", $mysqli->error);
          $this->existe = false;
        } else {
            $row = $result->fetch_assoc();
            $this->legajo = $row['legajo'];
            $this->nombre = $row['nombre'];
            $this->apellido = $row['apellido'];
            $this->tipo = $row['tipo'];
            $this->email = $row['email'];
            $this->direccion = $row['direccion'];
            $this->telefono = $row['telefono'];
            $this->estado = $row['estado'];
            $this->documentacion = $row['documentacion'];
            $this->password = $row['password'];
            $this->existe = true;
        }
        $sql = sprintf("SELECT * FROM `becas` WHERE dni = '%s'", 
            mysqli_real_escape_string($mysqli,$this->dni) 
        );
        if (!$result = $mysqli->query($sql)) {
            printf("Errormessage: %s\n", $mysqli->error);
        } else {
            while($row = $result->fetch_assoc()){
                array_push($this->becas, new Beca($row['dni'], $row['periodo'], $row['tipo']));
            }
        }
    }


    public function generarToken(){
        include_once('token.php');
        $this->token = new Token($this->dni);
        if ($this->token->getToken() != null && $this->token->guardar()) return true;
        else return false;
    }

    public function getToken(){
        return $this->token->getToken();
    }

    public function compro(){
        include '../sql.php';
        $sql = "SELECT * FROM `compras` WHERE dni = ".$this->dni." AND YEAR(fecha)=".date("Y")." AND MONTH(fecha) = ".date("m");
        if (!$result = $mysqli->query($sql)) {
            return false;
        } else {
            if ($result->num_rows > 0) {
                return true;
            } else {
                return false;
            }
        }
    }
}