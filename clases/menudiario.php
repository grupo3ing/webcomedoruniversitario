<?php

class MenuDiario {
	private $fecha_day;
	private $fecha_month;
	private $fecha_year;
	private $platoprincipal;
	private $guarnicion;
	private $salsa;
	private $postre;

	public function __construct($fecha_day, $fecha_month, $fecha_year, $platoprincipal, $guarnicion, $salsa, $postre){
		$this->fecha_day = $fecha_day;
		$this->fecha_month = $fecha_month;
		$this->fecha_year = $fecha_year;
		$this->platoprincipal = $platoprincipal;
		$this->guarnicion = $guarnicion;
		$this->salsa = $salsa;
		$this->postre = $postre;
	}

	static public function getMenusMes(){
		$month = date("n");
		$year = date("Y");
		include '../sql.php';
		$sql = "SELECT * FROM menusdiarios WHERE MONTH(fecha) = '" . $month . "' AND YEAR(fecha) = '" . $year . "'";
		$menus = [];
		if($result = $mysqli->query($sql)){
			while($row = $result->fetch_assoc()){
				$fecha = date_parse($row['fecha']);
				$fecha_day = $fecha['day'];
				$fecha_month = $fecha['month'];
				$fecha_year = $fecha['year'];
				$platoprincipal = $row['platoprincipal'];
				$guarnicion = $row['guarnicion'];
				$salsa = $row['salsa'];
				$postre = $row['postre'];
				array_push($menus, new MenuDiario($fecha_day, $fecha_month, $fecha_year, new Alimento($platoprincipal, ""), new Alimento($guarnicion, "Guarnicion"), new Alimento($salsa, "Salsa"), new Alimento($postre, "Postre")));
			}
		}
		return $menus;
	}

	public function getDay(){
		return $this->fecha_day;
	}

	public function getTooltip(){
		$s = "<html>";
		$s .= "<b>Plato principal</b>: " . $this->platoprincipal->getNombre() . "<br>";
		if ($this->salsa->getNombre()) {
			$s .= "<b>Salsa</b>: " . $this->salsa->getNombre() . "<br>";
		}
		if ($this->guarnicion->getNombre()){
			$s .= "<b>Guarnición</b>: " . $this->guarnicion->getNombre() . "<br>";
		}
		$s .= "<b>Postre</b>: " . $this->postre->getNombre();
		$s .= "</html>";
		return $s;
	}
}