-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema comedoruniversitario
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema comedoruniversitario
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `comedoruniversitario` DEFAULT CHARACTER SET utf8 ;
USE `comedoruniversitario` ;

-- -----------------------------------------------------
-- Table `comedoruniversitario`.`cliente`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `comedoruniversitario`.`cliente` ;

CREATE TABLE IF NOT EXISTS `comedoruniversitario`.`cliente` (
  `dni` VARCHAR(45) NOT NULL,
  `legajo` INT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `tipo` ENUM('estudiante', 'docente', 'particular', 'admin') NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `direccion` VARCHAR(45) NOT NULL,
  `telefono` VARCHAR(45) NOT NULL,
  `estado` ENUM('sancionado', 'activo', 'inactivo') NOT NULL DEFAULT 'inactivo',
  `documentacion` TINYINT NOT NULL DEFAULT 0,
  `becado` ENUM('completa', 'media', 'no') NOT NULL DEFAULT 'no',
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`dni`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `comedoruniversitario`.`vianda`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `comedoruniversitario`.`vianda` ;

CREATE TABLE IF NOT EXISTS `comedoruniversitario`.`vianda` (
  `precio` INT NOT NULL,
  `fecha` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`precio`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `comedoruniversitario`.`compra`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `comedoruniversitario`.`compra` ;

CREATE TABLE IF NOT EXISTS `comedoruniversitario`.`compra` (
  `estado` ENUM('pendiente', 'procesado', 'cancelado') NOT NULL,
  `dni` VARCHAR(45) NOT NULL,
  `fecha` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `precio_total` INT NOT NULL,
  `cantidad` INT NOT NULL,
  `vianda_precio` INT NOT NULL,
  PRIMARY KEY (`dni`, `fecha`),
  INDEX `fk_compra_vianda1_idx` (`vianda_precio` ASC),
  CONSTRAINT `fk_compra_cliente1`
    FOREIGN KEY (`dni`)
    REFERENCES `comedoruniversitario`.`cliente` (`dni`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_compra_vianda1`
    FOREIGN KEY (`vianda_precio`)
    REFERENCES `comedoruniversitario`.`vianda` (`precio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `comedoruniversitario`.`ticket`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `comedoruniversitario`.`ticket` ;

CREATE TABLE IF NOT EXISTS `comedoruniversitario`.`ticket` (
  `dni` VARCHAR(45) NOT NULL,
  `fecha` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`dni`, `fecha`),
  CONSTRAINT `fk_ticket_cliente1`
    FOREIGN KEY (`dni`)
    REFERENCES `comedoruniversitario`.`cliente` (`dni`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
-- begin attached script 'seed'

	/* Clientes */

INSERT INTO `cliente` (`legajo`, `nombre`, `apellido`, `tipo`, `dni`, `email`, `direccion`, `telefono`, `estado`, `documentacion`, `becado`, `password`) VALUES ('0000', 'Nombre', 'Apellido', 'estudiante', '00000001', 'correo@hotmail.com', 'Calle x', '0000000000', 'activo', '1', 'no', 'capocha');
INSERT INTO `cliente` (`legajo`, `nombre`, `apellido`, `tipo`, `dni`, `email`, `direccion`, `telefono`, `estado`, `documentacion`, `becado`, `password`) VALUES ('0000', 'Nombre', 'Apellido', 'docente', '00000002', 'correo@hotmail.com', 'Calle x', '0000000000', 'activo', '1', 'no', 'contraseña');
INSERT INTO `cliente` (`legajo`, `nombre`, `apellido`, `tipo`, `dni`, `email`, `direccion`, `telefono`, `estado`, `documentacion`, `becado`, `password`) VALUES ('0000', 'Nombre', 'Apellido', 'estudiante', '00000003', 'correo@hotmail.com', 'Calle x', '0000000000', 'activo', '1', 'completa', 'contraseña');
INSERT INTO `cliente` (`legajo`, `nombre`, `apellido`, `tipo`, `dni`, `email`, `direccion`, `telefono`, `estado`, `documentacion`, `becado`, `password`) VALUES ('0000', 'Nombre', 'Apellido', 'estudiante', '00000004', 'correo@hotmail.com', 'Calle x', '0000000000', 'activo', '1', 'completa', 'contraseña');
INSERT INTO `cliente` (`legajo`, `nombre`, `apellido`, `tipo`, `dni`, `email`, `direccion`, `telefono`, `estado`, `documentacion`, `becado`, `password`) VALUES ('0000', 'Nombre', 'Apellido', 'estudiante', '00000005', 'correo@hotmail.com', 'Calle x', '0000000000', 'activo', '1', 'completa', 'contraseña');
INSERT INTO `cliente` (`legajo`, `nombre`, `apellido`, `tipo`, `dni`, `email`, `direccion`, `telefono`, `estado`, `documentacion`, `becado`, `password`) VALUES ('0000', 'Nombre', 'Apellido', 'particular', '00000006', 'correo@hotmail.com', 'Calle x', '0000000000', 'activo', '0', 'no', 'contraseña');
INSERT INTO `cliente` (`legajo`, `nombre`, `apellido`, `tipo`, `dni`, `email`, `direccion`, `telefono`, `estado`, `documentacion`, `becado`, `password`) VALUES ('0000', 'Nombre', 'Apellido', 'estudiante', '00000007', 'correo@hotmail.com', 'Calle x', '0000000000', 'activo', '0', 'no', 'contraseña');
INSERT INTO `cliente` (`legajo`, `nombre`, `apellido`, `tipo`, `dni`, `email`, `direccion`, `telefono`, `estado`, `documentacion`, `becado`, `password`) VALUES ('5188', 'La maquina', 'del tiempo', 'admin', '39384405', 'antu.villegas@live.no', 'Calle 106 82 departamento 3', '2302543036', 'activo', '1', 'no', 'capocha');
INSERT INTO `cliente` (`legajo`, `nombre`, `apellido`, `tipo`, `dni`, `email`, `direccion`, `telefono`, `estado`, `documentacion`, `becado`, `password`) VALUES ('5188', 'Leandro Antú', 'Villegas', 'admin', '39384984', 'antu.villegas@live.no', 'Calle 106 82 departamento 3', '423561', 'activo', '1', 'no', '1001');
INSERT INTO `cliente` (`legajo`, `nombre`, `apellido`, `tipo`, `dni`, `email`, `direccion`, `telefono`, `estado`, `documentacion`, `becado`, `password`) VALUES ('0000', 'asdasd', 'dsadsa', 'estudiante', '40000000', 'antu.villegas@live.no', 'Calle 106 82 departamento 3', '423561', 'activo', '1', 'no', '1001');
INSERT INTO `cliente` (`legajo`, `nombre`, `apellido`, `tipo`, `dni`, `email`, `direccion`, `telefono`, `estado`, `documentacion`, `becado`, `password`) VALUES ('1234', 'Nico', 'Silvera', 'admin', '39386068', 'hola@live.no', 'Calle 16 82 departamento 3', '2302368067', 'activo', '1', 'no', 'capocha');

  	/* Tickets */

INSERT INTO `ticket` (`dni`, `fecha`) VALUES ('39384984', '2017-10-15 13:21:20');
INSERT INTO `ticket` (`dni`, `fecha`) VALUES ('39384984','2017-10-16 12:48:46');
INSERT INTO `ticket` (`dni`, `fecha`) VALUES ('39384984', '2017-10-17 13:21:20');
INSERT INTO `ticket` (`dni`, `fecha`) VALUES ('39384984','2017-10-18 12:48:46');
INSERT INTO `ticket` (`dni`, `fecha`) VALUES ('39384984', '2017-10-19 13:21:20');
INSERT INTO `ticket` (`dni`, `fecha`) VALUES ('39384984','2017-10-20 12:48:46');
INSERT INTO `ticket` (`dni`, `fecha`) VALUES ('39384984', '2017-10-21 13:21:20');
INSERT INTO `ticket` (`dni`, `fecha`) VALUES ('39384984','2017-10-22 12:48:46');
INSERT INTO `ticket` (`dni`, `fecha`) VALUES ('39384984','2017-10-23 12:48:46');
INSERT INTO `ticket` (`dni`, `fecha`) VALUES ('39384984', '2017-10-24 13:21:20');
INSERT INTO `ticket` (`dni`, `fecha`) VALUES ('39384984','2017-10-25 12:48:46');
INSERT INTO `ticket` (`dni`, `fecha`) VALUES ('39384984', '2017-10-26 13:21:20');

	/* Vianda */
    
INSERT INTO `vianda` (`precio`) VALUES (20);

  	/* Compras */

INSERT INTO `compra` (`estado`, `dni`, `fecha`,`precio_total`, `cantidad`, `vianda_precio`) VALUES ('procesado', '39384984', '2017-09-20 15:12:28', 400, 20, 20);
INSERT INTO `compra` (`estado`, `dni`, `fecha`,`precio_total`, `cantidad`, `vianda_precio`) VALUES ('procesado', '39386068', '2017-09-25 15:12:28', 400, 20, 20);
INSERT INTO `compra` (`estado`, `dni`, `fecha`,`precio_total`, `cantidad`, `vianda_precio`) VALUES ('procesado', '00000001', '2017-09-20 15:12:28', 400, 20, 20);
INSERT INTO `compra` (`estado`, `dni`, `fecha`,`precio_total`, `cantidad`, `vianda_precio`) VALUES ('procesado', '00000002', '2017-09-25 15:12:28', 400, 20, 20);
INSERT INTO `compra` (`estado`, `dni`, `fecha`,`precio_total`, `cantidad`, `vianda_precio`) VALUES ('procesado', '39384984', '2017-08-21 15:12:28', 400, 20, 20);
INSERT INTO `compra` (`estado`, `dni`, `fecha`,`precio_total`, `cantidad`, `vianda_precio`) VALUES ('procesado', '00000001', '2017-08-21 15:12:28', 400, 20, 20);
INSERT INTO `compra` (`estado`, `dni`, `fecha`,`precio_total`, `cantidad`, `vianda_precio`) VALUES ('procesado', '00000003', '2017-08-21 15:12:28', 400, 20, 20);
INSERT INTO `compra` (`estado`, `dni`, `fecha`,`precio_total`, `cantidad`, `vianda_precio`) VALUES ('procesado', '00000002', '2017-08-21 15:12:28', 400, 20, 20);
INSERT INTO `compra` (`estado`, `dni`, `fecha`,`precio_total`, `cantidad`, `vianda_precio`) VALUES ('procesado', '00000004', '2017-08-21 15:12:28', 400, 20, 20);
INSERT INTO `compra` (`estado`, `dni`, `fecha`,`precio_total`, `cantidad`, `vianda_precio`) VALUES ('procesado', '00000005', '2017-08-21 15:12:28', 400, 20, 20);
INSERT INTO `compra` (`estado`, `dni`, `fecha`,`precio_total`, `cantidad`, `vianda_precio`) VALUES ('procesado', '00000006', '2017-08-21 15:12:28', 400, 20, 20);
INSERT INTO `compra` (`estado`, `dni`, `fecha`,`precio_total`, `cantidad`, `vianda_precio`) VALUES ('procesado', '00000007', '2017-08-21 15:12:28', 400, 20, 20);

INSERT INTO `compra` (`estado`, `dni`, `fecha`,`precio_total`, `cantidad`, `vianda_precio`) VALUES ('procesado', '39384984', '2017-07-21 15:12:28', 400, 20, 20);
INSERT INTO `compra` (`estado`, `dni`, `fecha`,`precio_total`, `cantidad`, `vianda_precio`) VALUES ('procesado', '39386068', '2017-07-21 15:12:28', 400, 20, 20);
INSERT INTO `compra` (`estado`, `dni`, `fecha`,`precio_total`, `cantidad`, `vianda_precio`) VALUES ('procesado', '39384405', '2017-07-21 15:12:28', 400, 20, 20);
INSERT INTO `compra` (`estado`, `dni`, `fecha`,`precio_total`, `cantidad`, `vianda_precio`) VALUES ('procesado', '39384984', '2017-10-25 15:12:28', 400, 20, 20);

  	/* Usuarios */
    
CREATE USER 'comedor'@'localhost' IDENTIFIED BY 'universitario';
GRANT ALL PRIVILEGES ON `comedoruniversitario`.* TO 'comedor'@'localhost' REQUIRE NONE WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
-- end attached script 'seed'
