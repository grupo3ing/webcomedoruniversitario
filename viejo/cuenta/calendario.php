 <?php
date_default_timezone_set("America/Argentina/Buenos_Aires");
$year = date("Y");
$month = date("n") + 1;
$monthName = getMonthName($month, $year);
$daysprevm = date("t", strtotime(getPrevMonth($month, $year) . "-01"));
$daysm = date("t", strtotime($year . "-" . $month . "-01"));
$firstday = date("N", strtotime($year . "-" . $month . "-01")); // del 1 (Lunes) al 7 (Domingo) 
$firstday = ($firstday == 7) ? 0 : $firstday; // Domingo es 0
$fila = 0;

function getPrevMonth($m, $y){
    $monprev = ($m == 1) ? 12 : $m - 1;
    $yearprev = ($monprev == 12) ? $y-1 : $y;
    return $yearprev . '-' . $monprev;
}
?>

<div class="calendar-container">
    <div class="view-month">
        <div class="title active">
            <span class="month-name"><?php echo $monthName; ?></span>
            <span class="date-year"><?php echo $year; ?></span>
        </div>
        <div class="grid">
            <div class="row rowweek week">
                <div class="cell"><span>D</span></div>
                <div class="cell"><span>L</span></div>
                <div class="cell"><span>M</span></div>
                <div class="cell"><span>M</span></div>
                <div class="cell"><span>J</span></div>
                <div class="cell"><span>V</span></div>
                <div class="cell"><span>S</span></div>
            </div>
            <?php
                if($firstday == 0){
                    echo '<div class="row rowweek">';
                    for($i = $daysprevm - 6; $i <= $daysprevm; $i++){
                        echo '<div class="cell past"><span title="Anterior mes">' . $i . '</span></div>';
                    }
                    echo '</div>';
                    $fila++;
                }
                echo '<div class="row rowweek">';
                $addedDays = 0;
                for($i = $daysprevm - $firstday + 1;$i<=$daysprevm;$i++){ // dias del mes anterior
                    echo '<div class="cell past"><span title="Anterior mes">' . $i . '</span></div>';
                    $addedDays++;
                }
                for($i = 1; $i <= 7 - $addedDays; $i++){
                    if(in_array($i, $laborable))
                      echo '<div class="cell habilitado"><span title="Día activo">' . $i . '</span></div>';
                    else
                      echo '<div class="cell inhabilitado"><span title="Día no activo">' . $i . '</span></div>';
                }
                echo '</div>';
                $fila++;
                $diasfila = 0;
                echo '<div class="row rowweek">';
                for($i; $i <= $daysm; $i++){
                    if(in_array($i, $laborable))
                      echo '<div class="cell habilitado"><span title="Día activo">' . $i . '</span></div>';
                    else
                      echo '<div class="cell inhabilitado"><span title="Día no activo">' . $i . '</span></div>';
                    $diasfila++;
                    if($diasfila == 7) {
                        $diasfila = 0;
                        echo '</div>';
                        $fila++;
                        echo '<div class="row rowweek">';
                    }
                }
                for($i = 1; $i <= 7 - $diasfila; $i++){
                    echo '<div class="cell past"><span title="Siguiente mes">' . $i . '</span></div>';
                }
                echo '</div>';
                $fila++;
                if($fila < 6) {
                    echo '<div class="row rowweek">';
                    for($j = 0; $j < 7; $j++){
                        echo '<div class="cell past"><span title="Siguiente mes">' . $i . '</span></div>';
                        $i++;
                    }
                    echo '</div>';
                }
            ?>
        </div>
    </div>
</div>