<?php

header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
header("Access-Control-Allow-Headers: X-Requested-With");
header('Content-Type: text/html; charset=utf-8');
header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');

if(isset($_POST['cliente'])) {
    $logged = false;
    if($_POST['cliente'] != 'null') {
        $logged = true;
        $clientedata = json_decode($_POST['cliente'], true);
    }

    function getMonthName($mo, $ye){
        $m = date("M", strtotime($ye . "-" . $mo . "-01"));
        switch($m){
            case 'Jan':
                return 'Enero';
            case 'Feb':
                return 'Febrero';
            case 'Mar':
                return 'Marzo';
            case 'Apr':
                return 'Abril';
            case 'May':
                return 'Mayo';
            case 'Jun':
                return 'Junio';
            case 'Jul':
                return 'Julio';
            case 'Aug':
                return 'Agosto';
            case 'Sep':
                return 'Septiembre';
            case 'Oct':
                return 'Octubre';
            case 'Nov':
                return 'Noviembre';
            case 'Dec':
                return 'Diciembre';
        }
    }

    $test = true;

    require_once(__DIR__.'/../lib/cliente.php');

    require_once __DIR__ . '/../sql.php';

    $cliente = new Cliente($clientedata['dni']);



    $sql = "SELECT * FROM `viandas` ORDER BY fecha DESC LIMIT 1";
    if (!$result = $mysqli->query($sql)) {
        printf("Errormessage: %s\n", $mysqli->error);
    } else {
        $row = $result->fetch_assoc();
        $cupo = $row['cupo'];
        $precio = $row['precio'];
    }

    if($cliente->getBeca() == 'media'){
        $precio = $precio / 2;
    }

    $year = date("Y");
    $month = date("n") + 1;
    $sql = "SELECT * FROM `compras` WHERE ( estado = 'procesado' OR estado = 'pendiente' ) AND MONTH(fecha) = '" . $month . "' AND YEAR(fecha) = '" . $year . "';";
    if (!$result = $mysqli->query($sql)) {
        printf("Errormessage: %s\n", $mysqli->error);
    } else {
        $compras = $cupo;
        while($row = $result->fetch_assoc()){
            $compras--;
        }
    }

    $json = file_get_contents('http://nolaborables.com.ar/api/v2/feriados/' . $year . '?formato=mensual');
    $data = json_decode($json,true);

    $feriados = [];

    foreach(array_keys($data[$month-1]) as $feriado){
        array_push($feriados,$feriado);
    }

    $sql = "SELECT `fecha` FROM `nolaborables` WHERE MONTH(fecha) = '" . $month . "' AND YEAR(fecha) = '" . $year . "';";
    if (!$result = $mysqli->query($sql)) {
        printf("Errormessage: %s\n", $mysqli->error);
    } else {
        while($row = $result->fetch_assoc()){
            array_push($feriados,date_parse($row['fecha'])['day']);
        }
    }

    $day = date("N", strtotime($year . "-" . $month . "-01")); // del 1 (Lunes) al 7 (Domingo) 
    $day = ($day == 7) ? 0 : $day; // Domingo es 0
    $daysm = date("t", strtotime($year . "-" . $month . "-01"));

    $findesemana = [];

    for( $i = 1; $i <= $daysm; $i++ ){
        if($day == 0 || $day == 6) {
            array_push($findesemana,$i);
        }
        $day = ($day == 6) ? 0 : $day+1;
    }

    $laborable = [];
    $cantidad = 0;
    for($i = 1; $i <= $daysm; $i++){
        if(!in_array($i,$feriados) && !in_array($i,$findesemana)) {
            $cantidad++;
            array_push($laborable,$i);
        }
    }

    $monthName = getMonthName($month, $year);

    $mensajePrecio = $precio . ' x' . $cantidad . 'u.';

    require_once "../lib/mercadopago.php";
    $mp = new MP("6031434788276995", "XWJBTt5Bz6Qs99Yel0LaS1BV1riPFuyO");
    $preference_data = array(
        "items" => array(
            array(
                "title" => "Abono mensual para el mes de " . $monthName,
                "currency_id" => "ARS",
                "category_id" => "Abono mensual del Comedor Universitario",
                "quantity" => 1,
                "unit_price" => intval($precio * $cantidad)
            )
        ),
        "payer" => array(
            array(
                "name" => $cliente->getNombre() . ' ' . $cliente->getApellido(),
                "email" => $cliente->getEmail(),
                "identification" => array(
                    "type" => 'DNI',
                    "number" => $cliente->getDni()
                )
            )
        )
    );
    $preference = $mp->create_preference($preference_data);

    function activo(){
        return date("j") >= 20 && date("j") <= 28;
    }
    ?>
    <?php if(activo() || $test): ?>
                <div class="section"></div>
                <div class="container">
                    <div class="section cuenta">
                        <div class="row">
                            <div class="col s12 l4">
                                <div class="card">
                                    <div class="card-content center">
                                        <span class="card-title">Disponibilidad</span>
                                        <h3><strong><?php echo $compras . '/' . $cupo; ?></strong></h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 l4">
                                <div class="card">
                                    <div class="card-content center">
                                        <span class="card-title">Precio unitario</span>
                                        <?php if(strlen($mensajePrecio) > 8): ?>
                                            <p style="font-size: 2.4rem;line-height: 110%;font-weight: 500;    margin: 1.46rem 0 1.168rem 0;"><strong><?php echo $mensajePrecio; ?></strong>
                                            </p>
                                        <?php else: ?>
                                            <p style="font-size:2.92rem;line-height: 110%;font-weight: 500;    margin: 1.46rem 0 1.168rem 0;"><strong><?php echo $mensajePrecio; ?></strong>
                                            </p>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 l4">
                                <div class="card">
                                    <div class="card-content center">
                                        <span class="card-title">Precio total</span>
                                        <h3><strong>$<?php echo $precio * $cantidad; ?></strong></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 l6 m8 offset-m2">
                                <?php include 'calendario.php'; ?>
                            </div>
                            <div class="col s12 l6 m8 offset-m2 center">
                                <br><div class="hide-on-med-and-down"><br><br><br><br></div><br><br>
                                    <a draggable="false" class="waves-green waves-effect waves-light btn white black-text" name="MP-Checkout" href="<?php echo $preference["response"]["sandbox_init_point"]; ?>"><i draggable="false" class="material-icons right green-text">attach_money</i>Comprar</a>
                                <br><br>
                                <a draggable="false" class="waves-red waves-effect waves-light btn white black-text" href="cuenta.html"><i draggable="false" class="material-icons right red-text">close</i>Cancelar</a>
                            </div>
                        </div>
                    </div>
                    <br><br>
                </div>
        <?php elseif($cliente->getBeca() == 'completa'): ?>
                <div class="section"></div>
                <div class="container">
                    <div class="section cuenta">
                        <div class="row">
                            <div class="col s12 l8 offset-l2">
                                <div class="card">
                                    <div class="card-content center">
                                        <span class="card-title">Usted posee una beca completa</span>
                                        <p>No es necesario que adquiera el abono mensual</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br><br>
                </div>
        <?php else: ?>
                <div class="section"></div>
                <div class="container">
                    <div class="section cuenta">
                        <div class="row">
                            <div class="col s12 l8 offset-l2">
                                <div class="card">
                                    <div class="card-content center">
                                        <span class="card-title">Compras cerradas</span>
                                        <p>Las compras estarán disponibles desde el <strong>20</strong> al <strong>28</strong> inclusive</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br><br>
                </div>
        <script src="js/mprender.js"></script>
<?php endif; ?>
        <script type="text/javascript">
            init();
            $('.preloader-background').delay(1700).fadeOut('slow');
            
            $('.preloader-wrapper')
                .delay(1700)
                .fadeOut();
            links();
        </script>
<?php } ?>