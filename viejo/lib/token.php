<?php

class Token {

	private $token;
	private $dni;

	public function __construct($dni) {
		$this->dni = $dni;
		$this->token = md5(uniqid($dni, true));
	}

	public function guardar(){
		include('../sql.php');
		$sql = "DELETE FROM tokens WHERE dni = " . $this->dni;
		$mysqli->query($sql);
		$sql = sprintf("INSERT INTO `tokens` (`dni`, `token`, `fecha` ) VALUES ('%s', '%s', DATE_ADD(NOW(), INTERVAL 1 DAY))", $this->dni, $this->token);
		if (!$result = $mysqli->query($sql)) {
	        return false;
        } else {
        	return true;
        }
	}

	public function getToken(){
		return $this->token;
	}

}

?>