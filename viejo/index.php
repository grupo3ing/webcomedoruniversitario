<!DOCTYPE html>
<html lang="es">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
	<title>Comedor Universitario - UNLPam</title>
	<link href="/css/icon.css" rel="stylesheet">
	<link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection" />
	<link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection" />
</head>

<body>
	<?php $sidenav = false; include 'header.php'; ?>
	<main style="padding-left:0px;">
		<div class="section no-pad-bot" id="index-banner">
			<div class="container">
				<br><br>
				<h1 class="header center orange-text">Página principal</h1>
				<div class="row center">
					<h5 class="header col s12 light">(Noticias, fotos, información extra)</h5>
				</div>
				<div class="row center">
					<a href="#" id="download-button" class="btn-large waves-effect waves-light orange">Botón</a>
				</div>
				<br><br>
			</div>
		</div>
		<div class="container">
			<div class="section">
				<!--   Icon Section   -->
				<div class="row">
					<div class="col s12 m4">
						<div class="icon-block">
							<h2 class="center red-text"><i class="material-icons">flash_on</i></h2>
							<h5 class="center">Speeds up development</h5>
							<p class="light">We did most of the heavy lifting for you to provide a default stylings that incorporate our custom components. Additionally, we refined animations and transitions to provide a smoother experience for developers.</p>
						</div>
					</div>
					<div class="col s12 m4">
						<div class="icon-block">
							<h2 class="center red-text"><i class="material-icons">group</i></h2>
							<h5 class="center">User Experience Focused</h5>
							<p class="light">By utilizing elements and principles of Material Design, we were able to create a framework that incorporates components and animations that provide more feedback to users. Additionally, a single underlying responsive system across all platforms
								allow for a more unified user experience.</p>
						</div>
					</div>
					<div class="col s12 m4">
						<div class="icon-block">
							<h2 class="center red-text"><i class="material-icons">settings</i></h2>
							<h5 class="center">Easy to work with</h5>
							<p class="light">We have provided detailed documentation as well as specific code examples to help new users get started. We are also always open to feedback and can answer any questions a user may have about Materialize.</p>
						</div>
					</div>
				</div>
			</div>
			<br><br>
		</div>
	</main>
	<?php include 'footer.php'; ?>
	<!--  Scripts-->
	<script src="js/jquery.min.js"></script>
	<script src="js/materialize.min.js"></script>
	<script src="js/init.js"></script>
</body>

</html>