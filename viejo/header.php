<?php

header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
header("Access-Control-Allow-Headers: X-Requested-With");
header('Content-Type: text/html; charset=utf-8');
header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');

if(isset($_POST['cliente']) && isset($_POST['sidenav'])){
	$logged = false;
	if ($_POST['sidenav'] == 'true') {
		$sidenav = true;
	} else {
		$sidenav = false;
	}
	if($_POST['cliente'] != 'null') {
		$logged = true;
		$cliente = json_decode($_POST['cliente'], true);
	}
?>
	<header>
		<div class="navbar-fixed">
			<nav role="navigation">
				<div class="container nav">
					<a draggable="false" id="logo-container" href="home.html" class="unselectable brand-logo">
						<img draggable="false" src="assets/logo.png">
						<span class="hide-on-med-and-down">Comedor Universitario</span>
					</a>

					<?php if($logged): ?>

					<ul class="right hide-on-med-and-down" style="height: 75px;">
						<li><a draggable="false" class="unselectable dropdown-button" data-activates="menuUsuario" href="javascript:;">Bienvenido, <strong><?php echo $cliente['nombre'] . " " . $cliente['apellido']; ?></strong><i class="material-icons right navicon">arrow_drop_down</i></a></li>
					</ul>
					<ul id='menuUsuario' class='dropdown-content'>
						<li><a draggable="false" class="dropdown-option unselectable" href="cuenta.html">Ir a Cuenta<i class="material-icons">person</i></a></li>
						<li class="divider"></li>
						<li><a draggable="false" class="dropdown-option unselectable" href="javascript:cerrarsesion();">Cerrar sesión<i class="material-icons">power_settings_new</i></a></li>
					</ul>

					<?php else: ?>

					<ul class="right hide-on-med-and-down">
						<li><a href="iniciarsesion.html">Iniciar sesión</a></li>
					</ul>

					<?php endif ?>


					<a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
				</div>
			</nav>
			<?php if($sidenav): ?>
				<ul id="nav-mobile" class="side-nav fixed">
					<li><a draggable="false" class="waves-effect waves-red" href="home.html">Inicio</a></li>
					<li><a draggable="false" class="waves-effect waves-red" href="javascript:cargar('cuenta')">Cuenta</a></li>
					<li><a draggable="false" class="waves-effect waves-red" href="javascript:cargar('comprar')">Comprar</a></li>
					<li class="disabled"><a draggable="false" class="waves-effect waves-red" href="#">Documentación</a></li>
					<li><a draggable="false" class="waves-effect waves-red" href="#">Menú del mes</a></li>
					<li><a draggable="false" class="waves-effect waves-red" href="#">Beca</a></li>
					<li><a draggable="false" class="waves-effect waves-red" href="#">Asistencia</a></li>
					<li class="disabled"><a draggable="false" class="waves-effect waves-red" href="#">Encuestas</a></li>
					<li class="hide-on-large-only"><a draggable="false" class="waves-effect waves-red" href="javascript:cerrarsesion();">Cerrar sesión</a></li>
				</ul>
			<?php else: ?>
				<ul id="nav-mobile" class="side-nav">
					<li><a draggable="false" class="waves-effect waves-red" href="home.html">Inicio</a></li>
					<?php if($logged): ?>
						<ul class="collapsible collapsible-accordion">
							<li class="bold">
								<a class="collapsible-header waves-effect waves-red">Cuenta</a>
								<div class="collapsible-body">
									<ul>
										<li><a draggable="false" class="waves-effect waves-red" href="cuenta.html">Ir a Cuenta</a></li>
										<li><a draggable="false" class="waves-effect waves-red" href="javascript:cerrarsesion();">Cerrar sesión</a></li>	
									</ul>
								</div>
							</li>
						</ul>
					<?php else: ?>
						<li><a draggable="false" class="waves-effect waves-red" href="iniciarsesion.html">Iniciar sesión</a></li>	
					<?php endif ?>
				</ul>
			<?php endif ?>
		</div>
	</header>
	<script type="text/javascript">
		init();
		$('.preloader-background').delay(1700).fadeOut('slow');
		
		$('.preloader-wrapper')
			.delay(1700)
			.fadeOut();
	</script>

<?php
}
?>