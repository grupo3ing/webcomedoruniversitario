<?php
require_once(__DIR__.'/lib/cliente.php');
header('Content-type: text/html; charset=utf-8');
if(session_status() == PHP_SESSION_NONE)
	session_start();
$logged = false;
if (isset($_SESSION['cliente'])) {
	$cliente = unserialize($_SESSION['cliente']);
    if ($cliente->existe()) header('Location: /cuenta');
}

if(isset($_POST['password']) && isset($_POST['dni'])) {
    $dni = $_POST['dni'];
    $password = $_POST['password'];
	$cliente = new Cliente($dni);
	
	if($cliente->getPassword() == $password) {
		$_SESSION['cliente'] = serialize($cliente);
		header('Location: /cuenta');
	} else {
        $error = "Combinación errónea";
	}
}
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
    <title>Comedor Universitario - UNLPam</title>
    <link href="/css/icon.css" rel="stylesheet">
    <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection" />
    <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection" />
    <style>
        body {
            display: flex;
            min-height: 100vh;
            flex-direction: column;
        }

        main {
            flex: 1 0 auto;
        }

        body {
            background: #fff;
        }

        .input-field input[type=date]:focus+label,
        .input-field input[type=text]:focus+label,
        .input-field input[type=email]:focus+label,
        .input-field input[type=password]:focus+label {
            color: #ff9800 !important;
        }

        .input-field input[type=date]:focus,
        .input-field input[type=text]:focus,
        .input-field input[type=email]:focus,
        .input-field input[type=password]:focus {
            border-bottom: 2px solid #ff9800 !important;
            box-shadow: none !important;
        }
    </style>
</head>

<body>
    <?php $sidenav = false; include 'header.php'; ?>
    <main style="padding-left:0;">
        <center>
            <div class="section"></div>
            <h5 class="orange-text">Ingresa tus datos para iniciar sesión</h5>
            <?php if (isset($error)): ?>
            
            <h3 class="red-text">Error: <?php echo $error; ?></h3>
            
            <?php endif ?>
            <div class="container">
                <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">

                    <form class="col s12" method="post" action="iniciarsesion">
                        <div class='row'>
                            <div class='col s12'>
                            </div>
                        </div>

                        <div class='row'>
                            <div class='input-field col s12'>
                                <input class='validate' type='text' name='dni' id='dni' />
                                <label for='email'>Ingresa tu DNI</label>
                            </div>
                        </div>

                        <div class='row'>
                            <div class='input-field col s12'>
                                <input class='validate' type='password' name='password' id='password' />
                                <label for='password'>Ingresa tu contraseña</label>
                            </div>
                            <label style='float: right;'>
								<a class='red-text' href='#!'><b>Olvidaste tu contraseña?</b></a>
							</label>
                        </div>

                        <br />
                        <center>
                            <div class='row'>
                                <button type='submit' name='btn_login' class='col s12 btn btn-large waves-effect orange'>Iniciar sesión</button>
                            </div>
                        </center>
                    </form>
                </div>
            </div>
            <a href="#!">Registrarse</a>
        </center>

        <div class="section"></div>
        <div class="section"></div>
    </main>
    <?php //include 'footer.php'; ?>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>