<footer class="page-footer red">
    <div class="container">
        <div class="row">
            <div class="col l6 s6">
                <img src="/assets/logo.png" height="100">
            </div>
            <div class="col l6 s6">
                <h5 class="white-text">Mapa de sitio</h5>
                <ul>
                    <li><a class="white-text" href="#!">Inicio</a></li>
                    <li><a class="white-text" href="#!">Cuenta</a></li>
                    <li><a class="white-text" href="#!">Contacto</a></li>
                    <li><a class="white-text" href="#!">Universidad Nacional de La Pampa<i style="position:absolute;font-size:1.1rem;margin-left:5px;margin-top:3px;" class="material-icons">launch</i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            Hecho por <span class="red-text text-lighten-3">Grupo 3</a>
        </div>
    </div>
</footer>