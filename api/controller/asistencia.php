<?php

$app->post('/inasistencia', function() use ($app) {
	$postVars = $app->request->post();
	if(isset($postVars['token']) && isset($postVars['desde']) && isset($postVars['hasta'])) {
	    require_once '../sql.php';
	    $sql = sprintf("SELECT dni FROM tokens WHERE token = '%s'", mysqli_real_escape_string($mysqli, $postVars['token']));
	    if (!$result = $mysqli->query($sql)) {
				$response['error'] = true;
	        	$response['message'] = 'Error en BD';
	        	echoResponse(500, $response);
	    } else {
	        $row = $result->fetch_assoc();
	        $dni = $row['dni'];
	        require_once('../clases/cliente.php');
	        $cliente = new Cliente($dni);
	        if (!$cliente->existe()) {
	            $response['error'] = true;
				$response['message'] = 'No se encontró cliente vinculado al token';
				echoResponse(201, $response);
	        } else {
	        	$desde = explode('/', $postVars['desde']);
	        	$hasta = explode('/', $postVars['hasta']);
	        	$desdetime = strtotime($desde[2] . "/" . $desde[1] . "/" . $desde[0]);
	        	$hastatime = strtotime($hasta[2] . "/" . $hasta[1] . "/" . $hasta[0]);
	        	$tickets = [];
	        	if($desdetime <= $hastatime && $desde[1] == $hasta[1] && $desde[2] == $hasta[2]){
	        		include_once '../clases/ticket.php';
	        		for($i = $desde[0]; $i<=$hasta[0]; $i++){
	        			$ticket = new Ticket($cliente->getDni(), $desde[2] . "/" . $desde[1] . "/" . $i);
	        			array_push($tickets, $ticket);
	        			$res = $ticket->guardar();
	        			if($res != ""){
	        				foreach ($tickets as $t) {
	        					$t->eliminar();
	        				}
	        				$response['error'] = true;
				        	$response['message'] = 'No se pudo guardar alguna inasistencia';
				        	echoResponse(201, $response);
				        	break;
	        			}
	        		}
	        		$response['success'] = true;
					echoResponse(200, $response);
	        	}
            }
        }
    }
});

?>