<?php

$app->post('/login', function () use ($app) {
	$postVars = $app->request->post();
	if(isset($postVars['dni']) && isset($postVars['password'])) {
		require_once '../sql.php';
		$dni = mysqli_real_escape_string($mysqli, $postVars['dni']);
		$password = mysqli_real_escape_string($mysqli, $postVars['password']);
		$sql = sprintf("SELECT * FROM clientes WHERE dni = '%s' AND password = '%s'", $dni, $password);
		if (!$result = $mysqli->query($sql)) {
		   	$response["error"] = true;
			$response["message"] = "Error al conectar al a base de datos.";
			echoResponse(500, $response);
		} else {
		    if($row = $result->fetch_assoc()){
		    	include_once(__DIR__.'/../../clases/cliente.php');
		    	$cliente = new Cliente($dni);
		    	if ($cliente->generarToken()){
			        $response['success'] = true;
			        $response['token'] = $cliente->getToken();
			        echoResponse(200, $response);
			    } else {
			    	$response["error"] = true;
					$response["message"] = "No se pudo generar correctamente el token.";
					echoResponse(500, $response);
			    }
		    } else {
		    	$response["error"] = true;
				$response["message"] = "Combinación errónea";
				echoResponse(201, $response);
		    }
		}
	}
});


?>