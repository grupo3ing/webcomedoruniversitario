﻿<?php

/*

---------- TEMPLATE ----------

$app->post('/menu', function() use ($app) {
	$postVars = $app->request->post();
	if(isset($postVars['token'])) {
	    require_once '../sql.php';
	    $sql = sprintf("SELECT dni FROM tokens WHERE token = '%s'", mysqli_real_escape_string($mysqli, $postVars['token']));
	    if (!$result = $mysqli->query($sql)) {
				$response['error'] = true;
	        	$response['message'] = 'Error en BD';
	        	echoResponse(500, $response);
	    } else {
	        $row = $result->fetch_assoc();
	        $dni = $row['dni'];
	        require_once('../clases/cliente.php');
	        require_once('../clases/beca.php');
	        $cliente = new Cliente($dni);
	        if (!$cliente->existe()) {
	            $response['error'] = true;
				$response['message'] = 'No se encontró cliente vinculado al token';
				echoResponse(201, $response);
	        } else {
	        	
	        	?>
	        	<main>
	        		<div class="section"></div>
	        		<div class="container">
	                    <div class="section cuenta">
	                        <div class="row">
	                            <div class="col s8 offset-s2 center">
					        		
					        	</div>
					        </div>
					    </div>
					</div>
	        	</main>
    			<script type="text/javascript">
					init();
		            $('.preloader-background').delay(1700).fadeOut('slow');
		            
		            $('.preloader-wrapper')
		                .delay(1700)
		                .fadeOut();
		            links();
				</script>
	        	<?php
	        }
	    }
	}
});

*/

$app->post('/header', function() use ($app) {
	$postVars = $app->request->post();
	if(isset($postVars['token']) && isset($postVars['sidenav'])){
		$logged = false;
		if ($postVars['sidenav'] == 'true') {
			$sidenav = true;
		} else {
			$sidenav = false;
		}

		require_once '../sql.php';

		if($postVars['token'] != 'null') {
		    $sql = sprintf("SELECT dni FROM tokens WHERE token = '%s'", mysqli_real_escape_string($mysqli, $postVars['token']));
		    if ($result = $mysqli->query($sql)) {
		        $row = $result->fetch_assoc();
		        $dni = $row['dni'];
		        require_once('../clases/cliente.php');
		        $cliente = new Cliente($dni);
		        if ($cliente->existe()) {
		        	$logged = true;
		        }
		    }
		}
	?>
		<header>
			<div class="navbar-fixed">
				<nav role="navigation">
					<div class="container nav">
						<a draggable="false" id="logo-container" href="home.html" class="unselectable brand-logo">
							<img draggable="false" src="assets/logo.png">
							<span class="hide-on-med-and-down">Comedor Universitario</span>
						</a>

						<?php if($logged): ?>

						<ul class="right hide-on-med-and-down" style="height: 75px;">
							<li><a draggable="false" class="unselectable dropdown-button" data-activates="menuUsuario" href="javascript:;">Bienvenido, <strong><?php echo $cliente->getNombre() . " " . $cliente->getApellido(); ?></strong><i class="material-icons right navicon">arrow_drop_down</i></a></li>
						</ul>
						<ul id='menuUsuario' class='dropdown-content'>
							<li><a draggable="false" class="dropdown-option unselectable" href="cuenta.html">Ir a Cuenta<i class="material-icons">person</i></a></li>
							<li class="divider"></li>
							<li><a draggable="false" class="dropdown-option unselectable" href="javascript:cerrarsesion();">Cerrar sesión<i class="material-icons">power_settings_new</i></a></li>
						</ul>

						<?php else: ?>

						<ul class="right hide-on-med-and-down">
							<li><a href="iniciarsesion.html">Iniciar sesión</a></li>
						</ul>

						<?php endif ?>


						<a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
					</div>
				</nav>
				<?php if($sidenav): ?>
					<ul id="nav-mobile" class="side-nav fixed">
						<li><a draggable="false" class="waves-effect waves-red" href="home.html">Inicio</a></li>
						<li><a draggable="false" class="waves-effect waves-red" href="javascript:cargar('cuenta')">Cuenta</a></li>
						<li><a draggable="false" class="waves-effect waves-red" href="javascript:cargar('comprar')">Comprar</a></li>
						<li class="disabled"><a draggable="false" class="waves-effect waves-red" href="#">Documentación</a></li>
						<li><a draggable="false" class="waves-effect waves-red" href="javascript:cargar('menu')">Menú del mes</a></li>
						<li><a draggable="false" class="waves-effect waves-red" href="javascript:cargar('becas')">Becas</a></li>
						<li><a draggable="false" class="waves-effect waves-red" href="javascript:cargar('asistencia')">Asistencia</a></li>
						<li class="disabled"><a draggable="false" class="waves-effect waves-red" href="#">Encuestas</a></li>
						<li class="hide-on-large-only"><a draggable="false" class="waves-effect waves-red" href="javascript:cerrarsesion();">Cerrar sesión</a></li>
					</ul>
				<?php else: ?>
					<ul id="nav-mobile" class="side-nav">
						<li><a draggable="false" class="waves-effect waves-red" href="home.html">Inicio</a></li>
						<?php if($logged): ?>
							<ul class="collapsible collapsible-accordion">
								<li class="bold">
									<a class="collapsible-header waves-effect waves-red">Cuenta</a>
									<div class="collapsible-body">
										<ul>
											<li><a draggable="false" class="waves-effect waves-red" href="cuenta.html">Ir a Cuenta</a></li>
											<li><a draggable="false" class="waves-effect waves-red" href="javascript:cerrarsesion();">Cerrar sesión</a></li>	
										</ul>
									</div>
								</li>
							</ul>
						<?php else: ?>
							<li><a draggable="false" class="waves-effect waves-red" href="iniciarsesion.html">Iniciar sesión</a></li>	
						<?php endif ?>
					</ul>
				<?php endif ?>
			</div>
		</header>
		<script type="text/javascript">
			init();
			$('.preloader-background').delay(1700).fadeOut('slow');
			
			$('.preloader-wrapper')
				.delay(1700)
				.fadeOut();
		</script>

	<?php
	}
});

$app->post('/menu', function() use ($app) {
	$postVars = $app->request->post();
	if(isset($postVars['token'])) {
	    require_once '../sql.php';
	    $sql = sprintf("SELECT dni FROM tokens WHERE token = '%s'", mysqli_real_escape_string($mysqli, $postVars['token']));
	    if (!$result = $mysqli->query($sql)) {
				$response['error'] = true;
	        	$response['message'] = 'Error en BD';
	        	echoResponse(500, $response);
	    } else {
	        $row = $result->fetch_assoc();
	        $dni = $row['dni'];
	        require_once('../clases/cliente.php');
	        $cliente = new Cliente($dni);
	        if (!$cliente->existe()) {
	            $response['error'] = true;
				$response['message'] = 'No se encontró cliente vinculado al token';
				echoResponse(201, $response);
	        } else {
	        	require_once('../clases/menudiario.php');
	        	require_once('../clases/alimento.php');
	        	$menus = MenuDiario::getMenusMes();
	        	$diasmenu = [];
	        	foreach ($menus as $menu) {
	        		array_push($diasmenu, $menu->getDay());
	        	}
	        	function getMenuDay($menus, $day){
	        		foreach ($menus as $menu) {
	        			if ($menu->getDay() == $day) {
	        				return $menu;
	        			}
	        		}
	        	}
	        	?>
	        	<main>
	        		<div class="section"></div>
	        		<div class="container">
	                    <div class="section cuenta">
	                    	<div class="row center">
	                    		<h4 style="font-weight: 200">Menú del mes</h4>
	                    	</div>
	                        <div class="row">
	                            <div class="col s12 l6 offset-l3 m8 offset-m2 center">
					        		<?php include 'calendario.php'; ?>
					        	</div>
					        </div>
					    </div>
					</div>
	        	</main>
    			<script type="text/javascript">
					init();
		            $('.preloader-background').delay(1700).fadeOut('slow');
		            
		            $('.preloader-wrapper')
		                .delay(1700)
		                .fadeOut();
		            links();
				</script>
	        	<?php
	        }
	    }
	}
});

$app->post('/becas', function() use ($app) {
	$postVars = $app->request->post();
	if(isset($postVars['token'])) {
	    require_once '../sql.php';
	    $sql = sprintf("SELECT dni FROM tokens WHERE token = '%s'", mysqli_real_escape_string($mysqli, $postVars['token']));
	    if (!$result = $mysqli->query($sql)) {
				$response['error'] = true;
	        	$response['message'] = 'Error en BD';
	        	echoResponse(500, $response);
	    } else {
	        $row = $result->fetch_assoc();
	        $dni = $row['dni'];
	        require_once('../clases/cliente.php');
	        require_once('../clases/beca.php');
	        $cliente = new Cliente($dni);
	        if (!$cliente->existe()) {
	            $response['error'] = true;
				$response['message'] = 'No se encontró cliente vinculado al token';
				echoResponse(201, $response);
	        } else {
	        	$becas = $cliente->getBecas();

	        	function cmp($b1, $b2) {
				    if ($b1->getPeriodo() == $b2->getPeriodo()) {
				        return 0;
				    }
				    return ($b1->getPeriodo() > $b2->getPeriodo()) ? -1 : 1;
				}

				usort($becas, "cmp");
	        	?>
	        	<main>
	        		<div class="section"></div>
	        		<div class="container">
	                    <div class="section cuenta">
	                    	<div class="row center">
	                    		<h4 style="font-weight: 200">Becas</h4>
	                    	</div>
	                        <div class="row">
	                            <div class="col s8 offset-s2 center">
					        		<div class="card">
					        			<div class="card-content">
					        				<?php if($becas): ?>
							        		<table class="highlight centered">
							        			<thead>
										          	<tr>
										              	<th>Período</th>
										              	<th>Tipo de beca</th>
										          	</tr>
										        </thead>
										        <tbody>
										        	<?php
										        		foreach ($becas as $beca) {
										        			echo "<tr>";
									        				echo "<td>" . $beca->getPeriodo() . "</td>";
									        				echo "<td>" . $beca->getTipo() . "</td>";
										        			echo "</tr>";
										        		}
										        	?>
										        </tbody>
							        		</table>
							        	<?php else: ?>
							        		<p>No hay registros de becas en su cuenta.</p>
							        	<?php endif; ?>
							        	</div>
							        </div>
					        	</div>
					        </div>
					    </div>
					</div>
	        	</main>
    			<script type="text/javascript">
					init();
		            $('.preloader-background').delay(1700).fadeOut('slow');
		            
		            $('.preloader-wrapper')
		                .delay(1700)
		                .fadeOut();
		            links();
				</script>
	        	<?php
	        }
	    }
	}
});

$app->post('/asistencia', function() use ($app) {
	$postVars = $app->request->post();
	if(isset($postVars['token'])) {

	    require_once '../sql.php';

	    $sql = sprintf("SELECT dni FROM tokens WHERE token = '%s'", mysqli_real_escape_string($mysqli, $postVars['token']));

	    if (!$result = $mysqli->query($sql)) {
	        echo "<script>cerrarsesion();</script>";
	    } else {
	        $row = $result->fetch_assoc();
	        $dni = $row['dni'];
	        require_once('../clases/cliente.php');
	        $cliente = new Cliente($dni);
	        if (!$cliente->existe()) {
	            echo "<script>cerrarsesion();</script>";
	        } else {
	        	$asistencias = [];

	        	$sql = "SELECT DAY(fecha) AS dia FROM tickets WHERE dni = " . $cliente->getDni() . " AND MONTH(fecha) = " . date("m") . " AND YEAR(fecha) = " . date("Y");
	        	$result = $mysqli->query($sql);
		        while($row = $result->fetch_assoc()){
		        	array_push($asistencias, $row['dia']);
		        }

	        	?>
	        	<main>
	        		<div class="section"></div>
	        		<div class="section"></div>
                	<a href="#inasistencia" style="margin-right: 3%;" class="modal-trigger waves-red waves-effect waves-light btn right white black-text"><i class="red-text material-icons left">notifications_active</i>Avisar inasistencia</a>
                	<div id="inasistencia" style="min-height: 85vh;" class="modal">
					    <div class="modal-content" style="min-height: 70vh;">
					      	<h4>Avisar inasistencia</h4>
					      	<br><br>
					      	<label for="desde">Selecciona fecha de inicio</label>
					      	<input id="desde" type="text" class="datepicker">
					      	<br><br>
					      	<label for="desde">Selecciona fecha de fin</label>
					      	<input id="hasta" type="text" class="datepicker">
					    </div>
					    <div class="modal-footer">
					      	<a id="botonguardar" href="javascript:inasistencia();" class="red-text modal-action  waves-effect waves-red btn-flat">Guardar</a>
					    </div>
				  	</div>
                	<div class="section"></div>
	        		<div class="container">
	                    <div class="section cuenta">
			        		<div class="row center">
			        			<h4 style="font-weight: 200">Asistencia</h4>
			        		</div>
	                        <div class="row">
	                        	<div class="col s12 l6 offset-l3 m8 offset-m2 center">
	                        		<?php include 'calendario.php'; ?>
	                        	</div>
	                        </div>
	                        <div class="row">
	                        	<div class="col s12 l6 offset-l3 m8 offset-m2">
			                        <span class="left" style="margin-right: 8px;
    margin-top: 2px;background-color:#ff9800;width: 16px;height: 16px;display:block;"></span> Asistencia<br>
			                        <span class="left" style="margin-right: 8px;
    margin-top: 2px;background-color:#f44336;width: 16px;height: 16px;display:block;"></span> Día activo<br>
    <span class="left" style="margin-right: 8px;
    margin-top: 2px;background-color:gray;width: 16px;height: 16px;display:block;"></span> Día no activo
			                	</div>
			                </div>
	                    </div>
	                </div>
	            </main>
	            <script type="text/javascript">
		            init();
		            $('.preloader-background').delay(1700).fadeOut('slow');
		            
		            $('.preloader-wrapper')
		                .delay(1700)
		                .fadeOut();
		            <?php 
		            $year = date("Y");
		        	$month = date('n', strtotime("-1 months", strtotime($year."-".date("n")."-01")));
		        	$days = date("t");
		        	$max = $year . "," . $month . "," . $days;
		        	$min = $year . "," . $month . "," . "01";
		        	?>
		            $('.datepicker').pickadate({
		            	min: new Date(<?php echo $min; ?>),
		            	max: new Date(<?php echo $max; ?>),
					    selectMonths: false, // Creates a dropdown to control month
					    selectYears: 0, // Creates a dropdown of 15 years to control year,
					    today: 'Hoy',
					    clear: 'Reiniciar',
					    close: 'Aceptar',
					    closeOnSelect: false // Close upon selecting a date,
					});
		            links();
		        </script>
	        	<?php
        	}
        }
    }
});

$app->post('/cuenta', function() use ($app) {
	$postVars = $app->request->post();
	if(isset($postVars['token'])) {

	    require_once '../sql.php';

	    $sql = sprintf("SELECT dni FROM tokens WHERE token = '%s'", mysqli_real_escape_string($mysqli, $postVars['token']));

	    if (!$result = $mysqli->query($sql)) {
	        echo "<script>cerrarsesion();</script>";
	    } else {
	        $row = $result->fetch_assoc();
	        $dni = $row['dni'];
	        require_once('../clases/cliente.php');
	        $cliente = new Cliente($dni);
	        if (!$cliente->existe()) {
	            echo "<script>cerrarsesion();</script>";
	        } else {
	        	?>
	        	<main>
	        		<div class="container">
	                    <div class="section cuenta">
	                        <div class="row">
	                            <div class="col m8 offset-m2 s12">
	                            	<div class="card" style="margin-top: 50px;">
	                            		<div class="card-image red" style="height: 5px;">
							            </div>
							            <div class="card-content">
		                            		<div class="center">
		                            			<img src="assets/user.png" class="circle" style="width: 30%;">
			                            		<br><br>
			                            		<h4><?php echo $cliente->getNombre() . " " . $cliente->getApellido(); ?></h4>
			                            		<br>
			                            	</div>
		                            		<ul class="collection">
											    <li class="collection-item">
											      	<h5>Legajo</h5>
											      	<p><?php echo $cliente->getLegajo(); ?></p>
											    </li>
											    <li class="collection-item">
											      	<a href="javascript:;" class="secondary-content"><i class="material-icons red-text">edit</i></a>
											      	<h5>Email</h5>
											      	<p><?php echo $cliente->getEmail(); ?></p>
											    </li>
											    <li class="collection-item">
											      	<a href="javascript:;" class="secondary-content"><i class="material-icons red-text">edit</i></a>
											      	<h5>Dirección</h5>
											      	<p><?php echo $cliente->getDireccion(); ?></p>
											    </li>
											    <li class="collection-item">
											      	<a href="javascript:;" class="secondary-content"><i class="material-icons red-text">edit</i></a>
											      	<h5>Teléfono</h5>
											      	<p><?php echo $cliente->getTelefono(); ?></p>
											    </li>
											    <li class="collection-item">
											      	<h5>Situación en el comedor</h5>
											      	<p><?php echo ucfirst($cliente->getEstado()); ?></p>
											    </li>
											    <li class="collection-item">
											      	<h5>Docuementación presentada</h5>
											      	<p><?php echo $cliente->getDocumentacion()==1 ? 'Sí' : 'No'; ?></p>
											    </li>
											    <li class="collection-item">
											      	<h5>Becado</h5>
											      	<p><?php echo $cliente->getBeca() == 'no' ? 'No' : 'Si'; ?></p>
											    </li>
											</ul>
										</div>
	                            	</div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </main>
                <script type="text/javascript">
		            init();
		            $('.preloader-background').delay(1700).fadeOut('slow');
		            
		            $('.preloader-wrapper')
		                .delay(1700)
		                .fadeOut();
		            links();
		        </script>

	        	<?php
	        }
	    }
	}
});

$app->post('/mp', function() use ($app) {
	$postVars = $app->request->post();
	if(isset($postVars['token'])) {

	    require_once '../sql.php';

	    $sql = sprintf("SELECT dni FROM tokens WHERE token = '%s'", mysqli_real_escape_string($mysqli, $postVars['token']));

	    if (!$result = $mysqli->query($sql)) {
	        echo "<script>cerrarsesion();</script>";
	    } else {
	        $row = $result->fetch_assoc();
	        $dni = $row['dni'];
	        require_once('../clases/cliente.php');
	        $cliente = new Cliente($dni);
	        if (!$cliente->existe()) {
	            echo "<script>cerrarsesion();</script>";
	        } else {
	        	?>
	        	<main class="blue" style="min-height: calc(100vh - 64px);">
	        		<div class="container">
	                    <div class="section cuenta">
	                        <div class="row">
	                            <div class="col m6 offset-m3 s8 offset-s2 l6 offset-l3 center">
	                            	<div class="card" style="padding: 7%; margin-top: 50px;">
		                            	<img src="assets/mercadopago.png" style="width: 75%;">
		                            	<br>
		                            	<div class="input-field col s12">
										    <select id="estado">
											    <option value="" disabled selected>Elegir estado</option>
											    <option value="1">Pendiente</option>
											    <option value="2">Procesado</option>
										    </select>
										   	<label>Estado de pago:</label>
										</div>

		                            	<br>
		                            	<a draggable="false" class="waves-white waves-effect waves-light btn blue white-text" href="javascript:pagar();">Pagar</a>
		                            </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </main>
                <script type="text/javascript">
		            init();
		            $('.preloader-background').delay(1700).fadeOut('slow');
		            
		            $('.preloader-wrapper')
		                .delay(1700)
		                .fadeOut();
		            links();
		        </script>

	        	<?php
	        }
	    }
	}
});

$app->post('/comprar', function() use ($app) {
	$postVars = $app->request->post();
	if(isset($postVars['token'])) {

	    require_once '../sql.php';

	    $sql = sprintf("SELECT dni FROM tokens WHERE token = '%s'", mysqli_real_escape_string($mysqli, $postVars['token']));

	    if (!$result = $mysqli->query($sql)) {
	        echo "<script>cerrarsesion();</script>";
	    } else {
	        $row = $result->fetch_assoc();
	        $dni = $row['dni'];
	        require_once('../clases/cliente.php');
	        $cliente = new Cliente($dni);
	        if (!$cliente->existe()) {
	            echo "<script>cerrarsesion();</script>";
	        } else {

	            $test = true;

	            $sql = "SELECT * FROM `viandas` ORDER BY fecha DESC LIMIT 1";
	            if (!$result = $mysqli->query($sql)) {
	                printf("Errormessage: %s\n", $mysqli->error);
	            } else {
	                $row = $result->fetch_assoc();
	                $cupo = $row['cupo'];
	                $precio = $row['precio'];
	            }

	            if($cliente->getBeca() == 'media') {
	                $precio = $precio / 2;
	            }

	            $year = date("Y");
	            $month = date('n', strtotime("+1 months", strtotime($year."-".date('n')."-01")));
	            $sql = "SELECT * FROM `compras` WHERE ( estado = 'procesado' OR estado = 'pendiente' ) AND MONTH(fecha) = '" . date('n', strtotime("-1 months", strtotime($year."-".$month."-01"))) . "' AND YEAR(fecha) = '" . $year . "';";
	            if (!$result = $mysqli->query($sql)) {
	                printf("Errormessage: %s\n", $mysqli->error);
	            } else {
	                $compras = $cupo;
	                while($row = $result->fetch_assoc()) {
	                    $compras--;
	                }
	            }

	            if ($month == 1) {
		        	$year++;
		        }

	            $json = file_get_contents('http://nolaborables.com.ar/api/v2/feriados/' . $year . '?formato=mensual');
	            $data = json_decode($json,true);

	            $feriados = [];

	            foreach(array_keys($data[$month-1]) as $feriado) {
	                array_push($feriados,$feriado);
	            }

	            $sql = "SELECT `fecha` FROM `nolaborables` WHERE MONTH(fecha) = '" . $month . "' AND YEAR(fecha) = '" . $year . "';";
	            if (!$result = $mysqli->query($sql)) {
	                printf("Errormessage: %s\n", $mysqli->error);
	            } else {
	                while($row = $result->fetch_assoc()) {
	                    array_push($feriados,date_parse($row['fecha'])['day']);
	                }
	            }

	            $day = date("N", strtotime($year . "-" . $month . "-01")); // del 1 (Lunes) al 7 (Domingo) 
	            $day = ($day == 7) ? 0 : $day; // Domingo es 0
	            $daysm = date("t", strtotime($year . "-" . $month . "-01"));

	            $findesemana = [];

	            for( $i = 1; $i <= $daysm; $i++ ){
	                if($day == 0 || $day == 6) {
	                    array_push($findesemana,$i);
	                }
	                $day = ($day == 6) ? 0 : $day+1;
	            }

	            $laborable = [];
	            $cantidad = 0;
	            for($i = 1; $i <= $daysm; $i++){
	                if(!in_array($i,$feriados) && !in_array($i,$findesemana)) {
	                    $cantidad++;
	                    array_push($laborable,$i);
	                }
	            }

	            $mensajePrecio = $precio . ' x' . $cantidad . 'u.';

	            if($cliente->compro()) {
	            	require_once '../clases/compra.php';
	            	$ultimaCompra = Compra::buscarUltimaCompraxDni($cliente->getDni());
	            	$fechaCompra = $ultimaCompra->getFecha();
	            	$yearCompra = date('Y',strtotime($fechaCompra));
	            	$monthCompra = date('m',strtotime($fechaCompra));
            		if ($monthCompra == date("m") && $yearCompra == date("Y")){
            			$estadoCompra = $ultimaCompra->getEstado();
            		}
	            }

	            function activo() {
	                return date("j") >= 20 && date("j") <= 28;
	            }
	?>
	    <?php if((activo() || $test) && $cliente->getBeca() != 'completa' && $cliente->getEstado() != 'sancionado'): ?>
	    		<main>
	                <div class="section"></div>
	                <div class="container">
	                    <div class="section cuenta">
	                    	<div class="row center">
	                    		<h4 style="font-weight: 200">Compra de abono mensual</h4>
	                    	</div>
	                        <div class="row">
	                            <div class="col s12 l4">
	                                <div class="card">
	                                    <div class="card-content center">
	                                        <span class="card-title">Disponibilidad</span>
	                                        <h3><strong><?php echo $compras . '/' . $cupo; ?></strong></h3>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="col s12 l4">
	                                <div class="card">
	                                    <div class="card-content center">
	                                        <span class="card-title">Precio unitario</span>
	                                        <?php if(strlen($mensajePrecio) > 8): ?>
	                                            <p style="font-size: 2.4rem;line-height: 110%;font-weight: 500;    margin: 1.46rem 0 1.168rem 0;"><strong><?php echo $mensajePrecio; ?></strong>
	                                            </p>
	                                        <?php else: ?>
	                                            <p style="font-size:2.92rem;line-height: 110%;font-weight: 500;    margin: 1.46rem 0 1.168rem 0;"><strong><?php echo $mensajePrecio; ?></strong>
	                                            </p>
	                                        <?php endif; ?>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="col s12 l4">
	                                <div class="card">
	                                    <div class="card-content center">
	                                        <span class="card-title">Precio total</span>
	                                        <h3><strong>$<?php echo $precio * $cantidad; ?></strong></h3>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col s12 l6 m8 offset-m2">
	                                <?php include 'calendario.php'; ?>
	                            </div>
	                            <div class="col s12 l6 m8 offset-m2 center">
	                                <br><div class="hide-on-med-and-down"><br><br><br><br></div><br><br>
	                                <?php if (!$cliente->compro()): ?>
		                                <?php if ($cupo>0): ?>
		                                    <a draggable="false" class="waves-green waves-effect waves-light btn white black-text" name="MP-Checkout" href="javascript:cargar('mp');"><i draggable="false" class="material-icons right green-text">attach_money</i>Comprar</a>
		                                <?php else: ?>
		                                	<a draggable="false" class="disabled waves-green waves-effect waves-light btn white black-text" name="MP-Checkout" href="javascript:;"><i draggable="false" class="material-icons right green-text">attach_money</i>Comprar</a>
		                                <?php endif; ?>
		                                <br><br>
		                                <a draggable="false" class="waves-red waves-effect waves-light btn white black-text" href="javascript:cargar('cuenta');"><i draggable="false" class="material-icons right red-text">close</i>Cancelar</a>
		                            <?php elseif ($estadoCompra == 'procesado'): ?>
		                            	<h5>Estado de la compra</h5>
		                            	<a draggable="false" class="waves-green waves-effect waves-light btn white black-text" href="javascript:;"><i draggable="false" class="material-icons right green-text">done</i>Procesado</a>
		                            <?php elseif ($estadoCompra == 'pendiente'): ?>
		                            	<h5>Estado de la compra</h5>
		                            	<a draggable="false" class="waves-orange waves-effect waves-light btn white black-text" href="javascript:;"><i draggable="false" class="material-icons right orange-text">info</i>Pendiente</a>
		                           	<?php endif; ?>
	                            </div>
	                        </div>
	                    </div>
	                    <br><br>
	                </div>
	        <?php elseif($cliente->getBeca() == 'completa'): ?>
	        	<main>
	                <div class="section"></div>
	                <div class="container">
	                    <div class="section cuenta">
	                        <div class="row">
	                            <div class="col s12 l8 offset-l2">
	                                <div class="card">
	                                    <div class="card-content center">
	                                        <span class="card-title">Usted posee una beca completa</span>
	                                        <p>No es necesario que adquiera el abono mensual</p>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <br><br>
	                </div>
	            </main>
	        <?php elseif($cliente->getEstado() == 'sancionado'): ?>
	        	<main>
	                <div class="section"></div>
	                <div class="container">
	                    <div class="section cuenta">
	                        <div class="row">
	                            <div class="col s12 l8 offset-l2">
	                                <div class="card">
	                                    <div class="card-content center">
	                                        <p class="card-title">Usted posee una sanción. Diríjase al personal de rectorado para regularizar su situación o informarse acerca de la misma.</p>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <br><br>
	                </div>
	            </main>
	        <?php else: ?>
	        	<main>
	                <div class="section"></div>
	                <div class="container">
	                    <div class="section cuenta">
	                        <div class="row">
	                            <div class="col s12 l8 offset-l2">
	                                <div class="card">
	                                    <div class="card-content center">
	                                        <span class="card-title">Compras cerradas</span>
	                                        <p>La compra mensual estará disponible desde el <strong>20</strong> al <strong>28</strong> inclusive.</p>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <br><br>
	                </div>
	            </main>
	<?php endif; ?>
	        <!--<script src="js/mprender.js"></script>-->
	        <script type="text/javascript">
	            init();
	            $('.preloader-background').delay(1700).fadeOut('slow');
	            
	            $('.preloader-wrapper')
	                .delay(1700)
	                .fadeOut();
	            links();
	        </script>
	<?php
	        }
	    }
	}
});

?>