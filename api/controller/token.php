<?php
$app->post('/token', function () use ($app){
	$postVars = $app->request->post();
	if(isset($postVars['token'])) {
		require_once '../sql.php';
		$token = mysqli_real_escape_string($mysqli, $postVars['token']);
		$sql = sprintf("SELECT dni FROM tokens WHERE token = '%s'", mysqli_real_escape_string($mysqli, $token));
		if (!$result = $mysqli->query($sql)) {
		   	$response["error"] = true;
			$response["message"] = "Error al conectar al a base de datos para buscar token.";
			echoResponse(500, $response);
		} else {
		    if($row = $result->fetch_assoc()){
		    	$dni = $row['dni'];
		    	$sql = sprintf("SELECT * FROM clientes WHERE dni = '%s'", mysqli_real_escape_string($mysqli, $dni));
		    	if (!$result = $mysqli->query($sql)) {
				   	$response["error"] = true;
					$response["message"] = "Error al conectar al a base de datos para buscar cliente.";
					echoResponse(500, $response);
				} else {
					if($row = $result->fetch_assoc()){
				        $response['cliente'] = array_slice($row, 0, 10);
				        $response["success"] = true;
				        echoResponse(200, $response);
				    } else {
				    	$response["error"] = true;
						$response["message"] = "No se encontró ningún usuario vinculado a ese token.";
						echoResponse(201, $response);
				    }
			    }
		    } else {
		    	$response["error"] = true;
				$response["message"] = "No se encontró ningún login con ese token.";
				echoResponse(201, $response);
		    }
		}
	}
});

?>