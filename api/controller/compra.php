<?php

$app->post('/compra', function() use ($app) {
	$postVars = $app->request->post();
	if(isset($postVars['token']) && isset($postVars['estado'])) {
		$estado = $postVars['estado'];		
		if($estado == 'procesado' || $estado == 'pendiente') {
		    require_once '../sql.php';

		    $sql = sprintf("SELECT dni FROM tokens WHERE token = '%s'", mysqli_real_escape_string($mysqli, $postVars['token']));
		    if (!$result = $mysqli->query($sql)) {
		        echo "<script>cerrarsesion();</script>";
		        $response['error'] = true;
			    echoResponse(201, $response);
		    } else {
		    	if ($result->num_rows > 0) {
			        $row = $result->fetch_assoc();
			        $dni = $row['dni'];
			        require_once('../clases/cliente.php');
			        $cliente = new Cliente($dni);
			        if (!$cliente->existe()) {
			            echo "<script>cerrarsesion();</script>";
			        } else {
			        	if (!$cliente->compro()) {
				        	require_once('../clases/compra.php');
				        	$id = generateRandomString();
				        	$precios = Compra::calcularPrecio($cliente->getDni());
				        	$compra = Compra::crear($id, $estado, $cliente->getDni(), $precios['cantidad'], $precios['precio_unitario']);
				        	if ($compra->guardar()) {
					        	$response['success'] = true;
					        	echoResponse(200, $response);
					        } else {
					        	$response['error'] = true;
					        	$response['message'] = 'No pudo guardarse';
					        	echoResponse(201, $response);
					        }
					    } else {
					    	$response['error'] = true;
				        	$response['message'] = 'Ya adquirió el abono mensual';
				        	echoResponse(201, $response);
					    }
			        }
			    } else {
			    	$response['error'] = true;
				    echoResponse(201, $response);
			    }
		    }
		}
	}
});

function generateRandomString($length = 10) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}

?>