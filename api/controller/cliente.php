<?php

$app->get('/cliente/:dni', function ($dni){
	include '../sql.php';
	$sql = sprintf("SELECT * FROM clientes WHERE dni = '%s'", mysqli_real_escape_string($mysqli, $dni));
	if (!$result = $mysqli->query($sql)) {
	   	$response["error"] = true;
		$response["message"] = "Error al conectar al a base de datos.";
		echoResponse(500, $response);
	} else {
	    if($row = $result->fetch_assoc()){
	        $response['cliente'] = array_slice($row, 0, 10);
	        echoResponse(200, $response);
	    } else {
	    	$response["error"] = true;
			$response["message"] = "No se encontró ningún cliente con ese DNI.";
			echoResponse(201, $response);
	    }
	}
});

$app->get('/clientes', function (){
	include '../sql.php';
	$sql = "SELECT * FROM clientes ORDER BY dni";
	if (!$result = $mysqli->query($sql)) {
	   	$response["error"] = true;
		$response["message"] = "Error al conectar al a base de datos.";
		echoResponse(500, $response);
	} else {
	    while($row = $result->fetch_assoc()){
	        $response[$row['dni']] = array_slice($row, 0, 10);
	    }
	    echoResponse(200, $response);
	}
});

?>