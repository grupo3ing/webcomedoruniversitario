 <?php

date_default_timezone_set("America/Argentina/Buenos_Aires");

function getMonthName($mo, $ye) {
    $m = date("M", strtotime($ye . "-" . $mo . "-01"));
    switch($m){
        case 'Jan':
            return 'Enero';
        case 'Feb':
            return 'Febrero';
        case 'Mar':
            return 'Marzo';
        case 'Apr':
            return 'Abril';
        case 'May':
            return 'Mayo';
        case 'Jun':
            return 'Junio';
        case 'Jul':
            return 'Julio';
        case 'Aug':
            return 'Agosto';
        case 'Sep':
            return 'Septiembre';
        case 'Oct':
            return 'Octubre';
        case 'Nov':
            return 'Noviembre';
        case 'Dec':
            return 'Diciembre';
    }
}

if (!isset($month)) {
    $month = date("n");
}
if (!isset($asistencias)) {
    $asistencias = [];
}
if (!isset($diasmenu)){
    $diasmenu = [];
}

include_once '../clases/compra.php';
$info = Compra::getDatos($month);

$year = date("Y");
if ($month == 1) {
    $year++;
}

$monthName = getMonthName($month, $year);
$daysprevm = date("t", strtotime(getPrevMonth($month, $year) . "-01"));
$daysm = date("t", strtotime($year . "-" . $month . "-01"));
$firstday = date("N", strtotime($year . "-" . $month . "-01")); // del 1 (Lunes) al 7 (Domingo) 
$firstday = ($firstday == 7) ? 0 : $firstday; // Domingo es 0
$fila = 0;

function getPrevMonth($m, $y){
    $monprev = ($m == 1) ? 12 : $m - 1;
    $yearprev = ($monprev == 12) ? $y-1 : $y;
    return $yearprev . '-' . $monprev;
}
?>

<div class="calendar-container">
    <div class="view-month">
        <div class="title active">
            <span class="month-name"><?php echo $monthName; ?></span>
            <span class="date-year"><?php echo $year; ?></span>
        </div>
        <div class="grid">
            <div class="row rowweek week">
                <div class="cell"><span>D</span></div>
                <div class="cell"><span>L</span></div>
                <div class="cell"><span>M</span></div>
                <div class="cell"><span>M</span></div>
                <div class="cell"><span>J</span></div>
                <div class="cell"><span>V</span></div>
                <div class="cell"><span>S</span></div>
            </div>
            <?php
                if($firstday == 0){
                    echo '<div class="row rowweek">';
                    for($i = $daysprevm - 6; $i <= $daysprevm; $i++){
                        echo '<div class="cell past tooltipped" data-tooltip="Mes anterior"><span>' . $i . '</span></div>';
                    }
                    echo '</div>';
                    $fila++;
                }
                echo '<div class="row rowweek">';
                $addedDays = 0;
                for($i = $daysprevm - $firstday + 1;$i<=$daysprevm;$i++){ // dias del mes anterior
                    echo '<div class="cell past tooltipped" data-tooltip="Mes anterior"><span>' . $i . '</span></div>';
                    $addedDays++;
                }
                for($i = 1; $i <= 7 - $addedDays; $i++){
                    if(in_array($i, $info['laborable']))
                        if(in_array($i, $asistencias)){
                            echo '<div class="cell presente tooltipped" data-tooltip="Presente"><span>' . $i . '</span></div>';
                        } else if (in_array($i, $diasmenu)){
                            echo '<div class="cell habilitado tooltipped" data-tooltip="' . getMenuDay($menus, $i)->getTooltip() . '"><span><b>' . $i . '</b></span></div>';
                        } else {
                            echo '<div class="cell habilitado tooltipped" data-tooltip="Día activo"><span>' . $i . '</span></div>';
                        }
                    else
                      echo '<div class="cell inhabilitado tooltipped" data-tooltip="Día no activo"><span>' . $i . '</span></div>';
                }
                echo '</div>';
                $fila++;
                $diasfila = 0;
                echo '<div class="row rowweek">';
                for($i; $i <= $daysm; $i++){
                    if(in_array($i, $info['laborable']))
                        if(in_array($i, $asistencias)){
                            echo '<div class="cell presente tooltipped" data-tooltip="Presente"><span>' . $i . '</span></div>';
                        } else if (in_array($i, $diasmenu)){
                            echo '<div class="cell habilitado tooltipped" data-tooltip="' . getMenuDay($menus, $i)->getTooltip() . '"><span><b>' . $i . '</b></span></div>';
                        } else {
                            echo '<div class="cell habilitado tooltipped" data-tooltip="Día activo"><span>' . $i . '</span></div>';
                        }
                    else
                      echo '<div class="cell inhabilitado tooltipped" data-tooltip="Día no activo"><span>' . $i . '</span></div>';
                    $diasfila++;
                    if($diasfila == 7) {
                        $diasfila = 0;
                        echo '</div>';
                        $fila++;
                        echo '<div class="row rowweek">';
                    }
                }
                for($i = 1; $i <= 7 - $diasfila; $i++){
                    echo '<div class="cell past tooltipped" data-tooltip="Mes siguiente"><span>' . $i . '</span></div>';
                }
                echo '</div>';
                $fila++;
                if($fila < 6) {
                    echo '<div class="row rowweek">';
                    for($j = 0; $j < 7; $j++){
                        echo '<div class="cell past tooltipped" data-tooltip="Mes siguiente"><span>' . $i . '</span></div>';
                        $i++;
                    }
                    echo '</div>';
                }
            ?>
        </div>
    </div>
</div>